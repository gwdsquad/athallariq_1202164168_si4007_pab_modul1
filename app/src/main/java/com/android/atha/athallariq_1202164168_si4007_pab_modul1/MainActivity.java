package com.android.atha.athallariq_1202164168_si4007_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText editText, editText2;
    Button klik;
    TextView review;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = (EditText) findViewById(R.id.editText);
        editText2 = (EditText) findViewById(R.id.editText2);
        klik = (Button) findViewById(R.id.klik);
        review = (TextView) findViewById(R.id.review);

        klik.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                String isieditText = editText.getText().toString();
                String isieditText2 = editText2.getText().toString();

                double editText = Double.parseDouble(isieditText);
                double editText2 = Double.parseDouble(isieditText2);

                double hasil = luasnya(editText, editText2);
                String output = String.valueOf(hasil);
                review.setText(output.toString());

            }
        });
    }
    public  double luasnya(double editText, double editText2){
        return (editText*editText2);
    }
}
